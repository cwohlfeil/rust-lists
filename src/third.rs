use std::rc::Rc;

// Rc is just like Box, but we can duplicate it, and its memory will only be freed when all the Rc's derived from are dropped. Unfortunately, we can only Deref an Rc, so we can't implement IntoIter or IterMut for this type. We only have shared access to elements.

pub struct List<T> {
    head: Link<T>,
}

type Link<T> = Option<Rc<Node<T>>>;

struct Node<T> {
    elem: T,
    next: Link<T>,
}

pub struct Iter<'a, T:'a> {
    next: Option<&'a Node<T>>,
}

impl<T> List<T> {
    pub fn new() -> Self {
        List { head: None }
    }
    
    pub fn append(&self, elem: T) -> List<T> {
        // rather than moving a Box to be in the sublist, we just clone the head of the old list. We don't even need to match on the head, because Option exposes a Clone implementation that does exactly the thing we want.
        List { head: Some(Rc::new(Node {
            elem: elem,
            next: self.head.clone(),
        }))}
    }
    
    pub fn tail(&self) -> List<T> {
        // takes a list and removes the whole list with the first element removed. All that is is cloning the second element in the list (if it exists)
        List { head: self.head.as_ref().and_then(|node| node.next.clone()) } // we can just use and_then to let us return an Option
    }
    
    pub fn head(&self) -> Option<&T> {
        // returns a reference to the first element
        self.head.as_ref().map(|node| &node.elem )
    }
    
     pub fn iter<'a>(&'a self) -> Iter<'a, T> {
        Iter { next: self.head.as_ref().map(|node| &**node) }
    }
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.next.map(|node| {
            self.next = node.next.as_ref().map(|node| &**node);
            &node.elem
        })
    }
}

/* first (O(n)) solution for Drop
impl<T> Drop for List<T> {
    fn drop(&mut self) {
        let mut cur_list = self.head.take(); // Steal the list's head
        while let Some(node) = cur_list {
            cur_list = node.next.clone(); // Clone the current node's next node.
            // Node dropped here. If the old node had refcount 1, then it will be dropped and freed, but it won't be able to fully recurse and drop its child, because we hold another Rc to it.
        }
    }
}
*/ 

/* second (amortized O(1)) solution (only works on nightly). Might blow the stack, but much faster
impl<T> Drop for List<T> {
    fn drop(&mut self) {
        let mut head = self.head.take();
        while let Some(node) = head {
            if let Ok(mut node) = Rc::try_unwrap(node) {
                head = node.next.take();
            } else {
                break;
            }
        }
    }
} */

/* One reason to use an immutable linked list is to share data across threads. After all, shared mutable state is the root of all evil, and one way to solve that is to kill the mutable part forever. Except our list isn't thread safe at all. In order to be thread-safe, we need to fiddle with reference counts atomically. Otherwise, two threads could try to increment the reference count, and only one would happen. Then the list could get freed too soon. In order to get thread safety, we have to use Arc. Arc is completely identical to Rc except for the fact that reference counts are modified atomically. This has a bit of overhead if you don't need it, so Rust exposes both. All we need to do to make our list is replace every reference to Rc with std::sync::Arc.

You can't mess up thread-safety in Rust. The reason this is the case is because Rust models thread-safety in a first-class way with two traits: Send and Sync. A type is Send if it's safe to move to another thread. A type is Sync if it's safe to share between multiple threads. That is, if T is Sync, &T is Send. Safe in this case means it's impossible to to cause data races, (not to be mistaken with the more general issue of race conditions).

These are marker traits, which means they're traits that provide absolutely no interface. You either are Send, or you aren't. It's just a property other APIs can require. If you aren't appropriately Send, then it's statically impossible to be sent to a different thread. Send and Sync are also automatically derived traits based on whether you are totally composed of Send and Sync types. Almost every type is Send and Sync. Most types are Send because they totally own their data. Most types are Sync because the only way to share data across threads is to put them behind a shared reference, which makes them immutable.

However there are special types that violate these properties: those that have interior mutability. Interior mutability types let you mutate through a shared reference. There are two major classes of interior mutability: cells, which only work in a single-threaded context; and locks, which work in a multi-threaded context. For obvious reasons, cells are cheaper when you can use them. There's also atomics, which are primitives that act like a lock. Rc and Arc both use interior mutability for their reference count. Rc just uses a cell, which means it's not thread safe. Arc uses an atomic, which means it is thread safe. Of course, you can't magically make a type thread safe by putting it in Arc. Arc only can derive thread-safety like any other types.
*/ 

#[cfg(test)]
mod test {
    use super::List;

    #[test]
    fn basics() {
        let list = List::new();
        assert_eq!(list.head(), None);

        let list = list.append(1).append(2).append(3);
        assert_eq!(list.head(), Some(&3));

        let list = list.tail();
        assert_eq!(list.head(), Some(&2));

        let list = list.tail();
        assert_eq!(list.head(), Some(&1));

        let list = list.tail();
        assert_eq!(list.head(), None);

        // Make sure empty tail works
        let list = list.tail();
        assert_eq!(list.head(), None);
    }
    
    #[test]
    fn iter() {
        let list = List::new().append(1).append(2).append(3);

        let mut iter = list.iter();
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&1));
    }
}