pub struct List<T> { // Generics!
    head: Link<T>,
}

type Link<T> = Option<Box<Node<T>>>; // use type alias and Option instead

struct Node<T> {
    elem: T,
    next: Link<T>,
}

// Iterators
// Rust has nothing like a yield statement, so we're going to have to implement the logic ourselves
// there's 3 different kinds of iterator each collection should endeavour to implement:
// IntoIter - T
// IterMut - &mut T
// Iter - &T

// Tuple structs are an alternative form of struct, useful for trivial wrappers around other types.
pub struct IntoIter<T>(List<T>);

// Now we're dealing with lifetimes. A lifetime is the name of a scope somewhere in a program. That's it. When a reference is tagged with a lifetime, we're saying that it has to be valid for that entire scope.
// Iter is generic over *some* lifetime, it doesn't care
pub struct Iter<'a, T: 'a> { // even the type parameter needs a lifetime
    next: Option<&'a Node<T>>, // hold a pointer to the node we want to yield next
}

// mutable references can't coexist, so it's notably harder to write an IterMut using safe code
pub struct IterMut<'a, T: 'a> { // We take Option<&mut> so we have exclusive access to the mutable reference
    next: Option<&'a mut Node<T>>,
}

impl<T> List<T> {
    pub fn new() -> Self {
        List { head: None }
    }
    
    pub fn push(&mut self, elem: T) {
        let new_node = Box::new(Node {
            elem: elem,
            next: self.head.take(), // use take instead
        });

        self.head = Some(new_node);
    }
    
    pub fn pop(&mut self) -> Option<T> {
        self.head.take().map(|node| { // use map instead
            let node = *node;
            self.head = node.next;
            node.elem
        })
    }
    
    pub fn peek(&self) -> Option<&T> {
        self.head.as_ref().map(|node| {
            &node.elem
        })
    }
    
    pub fn peek_mut(&mut self) -> Option<&mut T> {
        self.head.as_mut().map(|node| {
            &mut node.elem
        })
    }
    
    pub fn into_iter(self) -> IntoIter<T> {
        IntoIter(self)
    }
    
    // We can declare a fresh lifetime here for the *exact* borrow that creates the iter. Now &self needs to be valid as long as the Iter is around.
    // However, we can rely on lifetime elision instead
    pub fn iter(&self) -> Iter<T> {
        Iter { next: self.head.as_ref().map(|node| &**node) }
    }
    
    pub fn iter_mut(&mut self) -> IterMut<T> { // self is a shared reference, we can't get mutable references out of it
        IterMut { next: self.head.as_mut().map(|node| &mut **node) }
    }
}

impl<T> Drop for List<T> {
    fn drop(&mut self) {
        let mut cur_link = self.head.take();
        while let Some(mut boxed_node) = cur_link {
            cur_link = boxed_node.next.take();
        }
    }
}

impl<T> Iterator for IntoIter<T> {
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        self.0.pop() // access fields of a tuple struct numerically
    }
}

impl<'a, T> Iterator for Iter<'a, T> { // *Do* have a lifetime here, because Iter does have an associated lifetime
    type Item = &'a T; // Need it here too, this is a type declaration

    fn next(&mut self) -> Option<Self::Item> { // None of this needs to change, handled by the above. 
        self.next.map(|node| {
            self.next = node.next.as_ref().map(|node| &**node);
            &node.elem
        })
    }
}


impl<'a, T> Iterator for IterMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        self.next.take().map(|node| {
            self.next = node.next.as_mut().map(|node| &mut **node);
            &mut node.elem
        })
    }
}


#[cfg(test)]
mod test {
    use super::List; // new module, requires import
    
    #[test]
    fn basics() {
        let mut list = List::new();

        // Check empty list behaves right
        assert_eq!(list.pop(), None);

        // Populate list
        list.push(1);
        list.push(2);
        list.push(3);

        // Check normal removal
        assert_eq!(list.pop(), Some(3));
        assert_eq!(list.pop(), Some(2));

        // Push some more just to make sure nothing's corrupted
        list.push(4);
        list.push(5);

        // Check normal removal
        assert_eq!(list.pop(), Some(5));
        assert_eq!(list.pop(), Some(4));

        // Check exhaustion
        assert_eq!(list.pop(), Some(1));
        assert_eq!(list.pop(), None);
    }
    
    #[test]
    fn peek() {
        let mut list = List::new();
        assert_eq!(list.peek(), None);
        assert_eq!(list.peek_mut(), None);
        list.push(1); list.push(2); list.push(3);

        assert_eq!(list.peek(), Some(&3));
        assert_eq!(list.peek_mut(), Some(&mut 3));
    }
    
    #[test]
    fn into_iter() {
        let mut list = List::new();
        list.push(1); list.push(2); list.push(3);

        let mut iter = list.into_iter();
        assert_eq!(iter.next(), Some(3));
        assert_eq!(iter.next(), Some(2));
        assert_eq!(iter.next(), Some(1));
    }
    
    #[test]
    fn iter() {
        let mut list = List::new();
        list.push(1); list.push(2); list.push(3);

        let mut iter = list.iter();
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&1));
    }
    
    #[test]
    fn iter_mut() {
        let mut list = List::new();
        list.push(1); list.push(2); list.push(3);

        let mut iter = list.iter_mut();
        assert_eq!(iter.next(), Some(&mut 3));
        assert_eq!(iter.next(), Some(&mut 2));
        assert_eq!(iter.next(), Some(&mut 1));
    }
}